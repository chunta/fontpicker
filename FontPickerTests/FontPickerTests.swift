//
//  FontPickerTests.swift
//  FontPickerTests
//
//  Created by apple on 2021/7/21.
//

import XCTest

@testable import Mocker

@testable import FontPicker

class TestViewObject: NSObject, IFontPickerView {

    typealias CompletionBlock = () -> Void
    var viewModel: FontPickerViewModel?
    var completionHandler: CompletionBlock?
    var failHandler: CompletionBlock?
    func fontDidRegistedAtItemIndex(_ index: Int) {

    }

    func fontRegistrationFailed(_ index: Int) {

    }

    func requestFontListDone() {

        if let completionHandler = completionHandler {
            completionHandler()
        }
    }

    func requestFontListFail() {

        if let failHandler = failHandler {
            failHandler()
        }
    }
}

class FontPickerTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        UserDefaults.standard.removeObject(forKey: "https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity")
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        UserDefaults.standard.removeObject(forKey: "https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity")
    }

    func testViewModelRequestFontList() throws {

        let view: TestViewObject = TestViewObject.init()
        let viewModel: FontPickerViewModel = FontPickerViewModel.init()
        let mockDataObj: [String: Any] = [
            "kind": "webfonts#webfontList",
            "items": [
                [
                    "family": "Roboto",
                    "lastModified": "2021-07-26",
                    "files": [
                        "regular": "http://fonts.gstatic.com/s/stixtwotext/v1/YA9Gr02F12Xkf5whdwKf11l0jbKkeidMTtZ5Yihg2SOYWxFMN1WD.ttf"
                    ]
                ], [
                    "family": "Noto Sans JP",
                    "lastModified": "2020-11-12",
                    "files": [
                        "regular": "http://fonts.gstatic.com/s/notosansjp/v28/-F62fjtqLzI2JPCgQBnw7HFowAIO2lZ9hg.otf"
                    ]
                ]
            ]
        ]
        let mockData = try! JSONSerialization.data(withJSONObject: mockDataObj, options: .prettyPrinted)
        let mock = Mock(url: URL.init(string: viewModel.apiUrl)!, dataType: .json, statusCode: 200, data: [
            .get: mockData
        ])
        mock.register()
        let expectation = XCTestExpectation(description: "Download apple.com home page")
        let completionHandler = {
            XCTAssertTrue(viewModel.itemCount() > 0)
            expectation.fulfill()
        }
        view.completionHandler = completionHandler
        viewModel.view = view
        view.viewModel = viewModel
        viewModel.requestFontList(mock.url)

        // Wait until the expectation is fulfilled, with a timeout of 1 seconds.
        wait(for: [expectation], timeout: 1.0)
    }

    func testViewModelFail500ToRequestFontList() throws {

        let view: TestViewObject = TestViewObject.init()
        let viewModel: FontPickerViewModel = FontPickerViewModel.init()
        let mock = Mock(url: URL.init(string: viewModel.apiUrl)!, dataType: .json, statusCode: 500, data: [.get: Data()])
        mock.register()
        let expectation = XCTestExpectation(description: "Download apple.com home page")
        let failHandler = {
            XCTAssertTrue(viewModel.itemCount() == 0)
            expectation.fulfill()
        }
        view.failHandler = failHandler
        viewModel.view = view
        view.viewModel = viewModel
        viewModel.requestFontList(mock.url)

        // Wait until the expectation is fulfilled, with a timeout of 1 seconds.
        wait(for: [expectation], timeout: 1.0)
    }

}
