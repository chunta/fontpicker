//
//  ContainsTests.swift
//  FontPickerTests
//
//  Created by apple on 2021/7/27.
//

import XCTest

@testable import FontPicker

class ContainsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test1() throws {
        let target: Int = 1
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test6() throws {
        let target: Int = 6
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test7() throws {
        let target: Int = 7
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test9() throws {
        let target: Int = 9
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test10() throws {
        let target: Int = 10
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test47() throws {
        let target: Int = 47
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test100() throws {
        let target: Int = 100
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test147() throws {
        let target: Int = 147
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test700() throws {
        let target: Int = 700
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test777() throws {
        let target: Int = 777
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test989() throws {
        let target: Int = 989
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test1000() throws {
        let target: Int = 1000
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test7000() throws {
        let target: Int = 7000
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test7007() throws {
        let target: Int = 7007
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test7772() throws {
        let target: Int = 7772
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test8000() throws {
        let target: Int = 8000
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test10000() throws {
        let target: Int = 10000
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test12345() throws {
        let target: Int = 12345
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test13707() throws {
        let target: Int = 13707
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test17756() throws {
        let target: Int = 17756
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test77725() throws {
        let target: Int = 77725
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }

    func test987421() throws {
        let target: Int = 987421
        let count: Int = Solution.init().solution(7, target)
        let countBf: Int = SolutionBf.init().bruceForce(7, target)
        XCTAssertEqual(count, countBf)
    }
}
