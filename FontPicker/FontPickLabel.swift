//
//  FontPickLabel.swift
//  FontPicker
//
//  Created by apple on 2021/7/28.
//

import UIKit

class FontPickLabel: UILabel {

    var imageIcon: UIImageView!
    var prePanLocation: CGPoint = .zero
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageIcon = UIImageView.init(image: UIImage.init(named: "ExpandIcon"))
        self.addSubview(imageIcon)
        imageIcon.translatesAutoresizingMaskIntoConstraints = false
        imageIcon.backgroundColor = .black
        NSLayoutConstraint.activate([
            imageIcon.widthAnchor.constraint(equalToConstant: Style.iconSizeSmall.width),
            imageIcon.heightAnchor.constraint(equalToConstant: Style.iconSizeSmall.height),
            imageIcon.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            imageIcon.topAnchor.constraint(equalTo: self.topAnchor, constant: 0)
        ])

        self.backgroundColor = Style.colorLightWhite
        let panGestureMove: UIPanGestureRecognizer = UIPanGestureRecognizer.init(target: self,
                                                                                 action: #selector(onPanMoveView))
        self.addGestureRecognizer(panGestureMove)
        self.isUserInteractionEnabled = true

        let panGestureRotScal: UIPanGestureRecognizer = UIPanGestureRecognizer.init(target: self,
                                                                                    action: #selector(onPanRotScaleView))
        imageIcon.addGestureRecognizer(panGestureRotScal)
        imageIcon.isUserInteractionEnabled = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func onPanMoveView(_ gesture: UITapGestureRecognizer) {

        print("onPanMoveView")
        if gesture.state == .cancelled {
            prePanLocation = .zero
            return
        }
        let location: CGPoint = gesture.location(in: self.superview)
        if prePanLocation != .zero {
            let dif: CGPoint = CGPoint(x: location.x - prePanLocation.x, y: location.y - prePanLocation.y)
            let translationMat = CGAffineTransform.init(translationX: dif.x, y: dif.y)
            self.transform = self.transform.concatenating(translationMat)
        }
        prePanLocation = location
    }

    @objc func onPanRotScaleView(_ gesture: UITapGestureRecognizer) {
        print("onPanRotScaleView")
    }
}
