//
//  Style.swift
//  FontPicker
//
//  Created by apple on 2021/7/21.
//

import Foundation
import UIKit

struct Style {

    static let spaceTiny: CGFloat = 3.0
    static let spaceSmall: CGFloat = 6.0
    static let spaceMedium: CGFloat = 14.0
    static let spaceLarge: CGFloat = 26.0
    static let colorWhite: UIColor = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    static let colorLightWhite: UIColor = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.9)
    static let colorLightBlue: UIColor = UIColor.init(red: 115.0 / 255.0, green: 215.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    static let colorLightPink: UIColor = UIColor.init(red: 255.0 / 255.0, green: 187.0 / 255.0, blue: 218.0 / 255.0, alpha: 1.0)
    static let colorLightGreyTransparent: UIColor = UIColor.init(red: CGFloat(227.0 / 255.0),
                                                            green: CGFloat(227.0 / 255.0),
                                                            blue: CGFloat(227.0 / 255.0),
                                                            alpha: 0.7)
    static let colorActivityIndicator: UIColor = UIColor.init(red: CGFloat(27.0 / 255.0),
                                                              green: CGFloat(27.0 / 255.0),
                                                              blue: CGFloat(27.0 / 255.0),
                                                              alpha: 0.7)
    static let colorDarkGray: UIColor = UIColor.init(red: CGFloat(127.0 / 255.0),
                                                     green: CGFloat(127.0 / 255.0),
                                                     blue: CGFloat(127.0 / 255.0),
                                                     alpha: 0.8)
    static let fontSizeMediuem: CGFloat = 20.0
    static let fontSizeLarge: CGFloat = 30.0
    static let fontMedium: UIFont = UIFont.systemFont(ofSize: Style.fontSizeMediuem)
    static let fontLarge: UIFont = UIFont.systemFont(ofSize: Style.fontSizeLarge)
    static let fontMinScale: CGFloat = 0.1

    static let iconSizeSmall: CGSize = CGSize(width: 16, height: 16)
    static let IconSizeMedium: CGSize = CGSize(width: 28, height: 28)
    static let iconSizeLarge: CGSize = CGSize(width: 50, height: 50)
}

extension UIActivityIndicatorView {
    func assignColor(_ color: UIColor) {
        style = .whiteLarge
        self.color = color
    }
}
