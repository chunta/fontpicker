//
//  FontPickerModel.swift
//  FontPicker
//
//  Created by apple on 2021/7/21.
//

import Foundation

struct FontItem: Codable {

    var family: String
    var files: [String: String]
    var lastModified: String
    var lastDownload: Int?
    var fileExtension: String?
}

struct FontResponse: Codable {
    var kind: String
    var items: [FontItem]
}
