//
//  ViewController.swift
//  FontPicker
//
//  Created by apple on 2021/7/21.
//

import UIKit

class FontPickerView: UIViewController {

    var viewModel: FontPickerViewModel!
    var demolabel: UILabel!
    let defaultDemoString: String = "Hello"
    var inputField: UITextField!
    var inputFiledOverlay: UIView!
    var activityIndicator: UIActivityIndicatorView!
    var canvasView: UIView!
    var collectionView: UICollectionView!
    var fontSelectedIndex: Int = -1
    let cellNumRow: CGFloat = 3.0
    let cellNumCol: CGFloat = 3.0
    let cellInternalSpace: CGFloat = 6.0
    let collectionWHRatio: CGFloat = 1.999 / 3.0 // (Height / Width)
    let naviButtonHeight: CGFloat = 48.0
    var naviButtonLeft: UIButton!
    var naviButtonMiddle: UIButton!
    var naviButtonRight: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = String(describing: FontPickerView.self)

        self.view.backgroundColor = .white

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.minimumLineSpacing = cellInternalSpace
        layout.minimumInteritemSpacing = cellInternalSpace
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        self.view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = Style.colorLightBlue
        collectionView.decelerationRate = .fast
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            collectionView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -naviButtonHeight),
            collectionView.heightAnchor.constraint(equalTo: collectionView.widthAnchor, multiplier: collectionWHRatio)
        ])
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(FontPickerCell.self, forCellWithReuseIdentifier: FontPickerCell.cellId)

        activityIndicator = UIActivityIndicatorView.init(frame: .zero)
        self.view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor)
        ])
        activityIndicator.assignColor(Style.colorActivityIndicator)
        activityIndicator.style = .whiteLarge
        activityIndicator.startAnimating()

        inputField = UITextField.init(frame: .zero)
        inputField.delegate = self
        inputField.addTarget(self, action: #selector(inputTextChanged), for: .editingChanged)
        inputField.font = Style.fontLarge
        inputField.textColor = Style.colorLightBlue
        inputField.layer.borderWidth = 1
        inputField.placeholder = "Type Here"
        inputField.textAlignment = .center
        self.view.addSubview(inputField)
        inputField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            inputField.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: Style.spaceMedium),
            inputField.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -Style.spaceMedium),
            inputField.bottomAnchor.constraint(equalTo: self.collectionView.topAnchor, constant: -Style.spaceMedium)
        ])

        inputFiledOverlay = UIView.init(frame: .zero)
        inputFiledOverlay.backgroundColor = Style.colorWhite
        inputFiledOverlay.layer.borderWidth = 1
        inputFiledOverlay.layer.borderColor = Style.colorLightGreyTransparent.cgColor
        self.view.insertSubview(inputFiledOverlay, belowSubview: inputField)
        inputFiledOverlay.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            inputFiledOverlay.topAnchor.constraint(equalTo: inputField.topAnchor, constant: -Style.spaceMedium),
            inputFiledOverlay.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            inputFiledOverlay.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            inputFiledOverlay.bottomAnchor.constraint(equalTo: self.collectionView.topAnchor, constant: 0)
        ])

        // The view where we put the demo label
        canvasView = UIView.init(frame: .zero)
        self.view.addSubview(canvasView)
        canvasView.clipsToBounds = true
        canvasView.layer.borderWidth = 1
        canvasView.translatesAutoresizingMaskIntoConstraints = false
        canvasView.backgroundColor = Style.colorLightPink
        NSLayoutConstraint.activate([
            canvasView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0),
            canvasView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            canvasView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            canvasView.bottomAnchor.constraint(equalTo: self.inputFiledOverlay.topAnchor, constant: 0)
        ])

        demolabel = UILabel.init(frame: CGRect(x: 0, y: 120, width: 100, height: 60))
        canvasView.addSubview(demolabel)
        demolabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            demolabel.leadingAnchor.constraint(equalTo: canvasView.leadingAnchor, constant: Style.spaceLarge),
            demolabel.trailingAnchor.constraint(equalTo: canvasView.trailingAnchor, constant: -Style.spaceLarge),
            demolabel.centerXAnchor.constraint(equalTo: canvasView.centerXAnchor),
            demolabel.centerYAnchor.constraint(equalTo: canvasView.centerYAnchor)
        ])
        demolabel.layer.borderWidth = 1
        demolabel.text = "Hello"
        demolabel.textAlignment = .center
        demolabel.font = Style.fontLarge
        demolabel.sizeToFit()

        naviButtonLeft = UIButton.init(frame: .zero)
        self.view.addSubview(naviButtonLeft)
        naviButtonLeft.layer.borderWidth = 1
        naviButtonLeft.translatesAutoresizingMaskIntoConstraints = false
        naviButtonLeft.setTitle("L", for: .normal)
        naviButtonLeft.setTitleColor(Style.colorDarkGray, for: .normal)
        naviButtonLeft.addTarget(self, action: #selector(onNaviLeft), for: .touchDown)
        NSLayoutConstraint.activate([
            naviButtonLeft.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            naviButtonLeft.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.333),
            naviButtonLeft.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0),
            naviButtonLeft.heightAnchor.constraint(equalToConstant: naviButtonHeight)
        ])

        naviButtonMiddle = UIButton.init(frame: .zero)
        self.view.addSubview(naviButtonMiddle)
        naviButtonMiddle.layer.borderWidth = 1
        naviButtonMiddle.translatesAutoresizingMaskIntoConstraints = false
        naviButtonMiddle.setTitle("M", for: .normal)
        naviButtonMiddle.setTitleColor(Style.colorDarkGray, for: .normal)
        naviButtonMiddle.addTarget(self, action: #selector(onNaviMiddle), for: .touchDown)
        NSLayoutConstraint.activate([
            naviButtonMiddle.leadingAnchor.constraint(equalTo: naviButtonLeft.trailingAnchor, constant: 0),
            naviButtonMiddle.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.333),
            naviButtonMiddle.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0),
            naviButtonMiddle.heightAnchor.constraint(equalToConstant: naviButtonHeight)
        ])

        naviButtonRight = UIButton.init(frame: .zero)
        self.view.addSubview(naviButtonRight)
        naviButtonRight.layer.borderWidth = 1
        naviButtonRight.translatesAutoresizingMaskIntoConstraints = false
        naviButtonRight.setTitle("R", for: .normal)
        naviButtonRight.setTitleColor(Style.colorDarkGray, for: .normal)
        naviButtonRight.addTarget(self, action: #selector(onNaviRight), for: .touchDown)
        NSLayoutConstraint.activate([
            naviButtonRight.leadingAnchor.constraint(equalTo: naviButtonMiddle.trailingAnchor, constant: 0),
            naviButtonRight.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.333),
            naviButtonRight.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0),
            naviButtonRight.heightAnchor.constraint(equalToConstant: naviButtonHeight)
        ])

        viewModel.requestFontList()
        
        
    }
}

extension FontPickerView {

    @objc func onNaviLeft() {

        collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
    }

    @objc func onNaviMiddle() {
        let index: Int = Int(viewModel.itemCount() / 3)
        collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: true)
    }

    @objc func onNaviRight() {

        let index: Int = Int((viewModel.itemCount() / 3) * 2)
        collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: true)
    }
}

extension FontPickerView: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @objc func inputTextChanged(_ textField: UITextField) {

        demolabel.text = textField.text
    }
}

extension FontPickerView: IFontPickerView {

    func requestFontListDone() {

        collectionView.reloadData()
        activityIndicator.stopAnimating()
    }

    func requestFontListFail() {

        print("Fail")
        activityIndicator.stopAnimating()
    }

    func fontDidRegistedAtItemIndex(_ index: Int) {

        collectionView.reloadItems(at: [IndexPath.init(item: index, section: 0)])
    }

    func fontRegistrationFailed(_ index: Int) {

        if let cell: FontPickerCell = collectionView.cellForItem(at: IndexPath.init(item: index, section: 0)) as? FontPickerCell {
            cell.requestFontFail()
        }
    }
}

extension FontPickerView: UICollectionViewDelegate,
                          UICollectionViewDataSource,
                          UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return viewModel.itemCount()
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FontPickerCell.cellId,
                                                      for: indexPath) as! FontPickerCell
        cell.fillData(viewModel.itemAtItemIndex(indexPath.row),
                      viewModel.fontAtItemIndex(indexPath.row),
                      indexPath.row,
                      fontSelectedIndex)
        cell.backgroundColor = Style.colorLightBlue
        cell.layer.borderWidth = 1
        cell.layer.borderColor = Style.colorLightGreyTransparent.cgColor
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = (collectionView.bounds.width - cellInternalSpace * (cellNumCol + 1)) / cellNumCol
        let height: CGFloat = (collectionView.bounds.height - cellInternalSpace * (cellNumRow + 1)) / cellNumCol
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: cellInternalSpace, left: cellInternalSpace, bottom: cellInternalSpace, right: cellInternalSpace)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let fontState: FontFamilyState = viewModel.requestFontAtItemIndex(indexPath.row)
        if fontState == .requesting {

            // Display activity indicator
            if let cell: FontPickerCell = collectionView.cellForItem(at: indexPath) as? FontPickerCell {
                cell.requestFontStart()
            }
        } else {

            print("Has own")
            // Update the state of the cell at prev-index and
            // the cell at selected index
            let prevSelectedIndex: Int = fontSelectedIndex
            fontSelectedIndex = indexPath.row
            if prevSelectedIndex >= 0 {
                collectionView.reloadItems(at: [IndexPath.init(item: prevSelectedIndex, section: 0)])
            }
            collectionView.reloadItems(at: [IndexPath.init(item: fontSelectedIndex, section: 0)])

            // Apply the selected font to the demo label
            if let font: UIFont = viewModel.fontAtItemIndex(indexPath.row) {
                demolabel.font = font.withSize(Style.fontSizeLarge)
            }
        }
    }
}
