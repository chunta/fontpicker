//
//  Solution.swift
//  FontPicker
//
//  Created by apple on 2021/7/27.
//

import Foundation

class Solution {

    func solution(_ containNumber: Int, _ targetNumber: Int) -> Int {

        // Early return
        if targetNumber < containNumber {
            return 0
        }

        var targetNumber: Int = targetNumber

        //////////////////////////////////////////////////////////////////
        // Step00. Calculate value base.
        // ex: 10 - 1, 100 - 20, 1000 - 300
        //////////////////////////////////////////////////////////////////
        let digitLen: Int = String.init(format: "%d", targetNumber).count
        var digitBaseList: [Int] = []
        var digitBase: Int = 1
        for digit in 1...digitLen {
            digitBase = calculateDigitBase(digit, digitBase, containNumber, targetNumber)
            digitBaseList.append(digitBase)
        }
        print(digitBaseList)
        var sum: Int = 0
        let logLen: Int = Int(log10(Float(targetNumber)))
        var leftOver: Int = 0
        var count7: Int = 0

        //////////////////////////////////////////////////////////////////
        // Step01. Break down the targetNumber into the format like:
        // I * 10000, J * 1000s, K * 100s
        //////////////////////////////////////////////////////////////////
        if logLen >= 2 {

            for index in (2...logLen).reversed() {
                let powVal: Int =  Int(pow(10.0, Float(index)))
                let count: Int = targetNumber / powVal
                sum += calculateCount(count, containNumber, powVal, digitBaseList[index - 1])
                leftOver = targetNumber % powVal
                targetNumber -= count * powVal
                print("LeftOver \(count * powVal) \(count7)")
                sum += count * powVal * count7
                if count == containNumber {
                    count7 += 1
                }
            }
        } else {

            leftOver = targetNumber
        }
        //////////////////////////////////////////////////////////////////
        // Step02. Handle the leftOver 1 or 2 digit
        //////////////////////////////////////////////////////////////////
        print("count7 \(count7)")
        sum += leftOver * count7
        print("LeftOver: \(leftOver)")
        if leftOver > 0 {
            if leftOver < 10 {
                if leftOver >= containNumber {
                    sum += 1
                }
            } else {

                let digitCount: Int = leftOver / 10 // Ex: 75
                sum += digitCount
                print("digitCount \(digitCount)")
                if digitCount < containNumber {

                    let closest: Int = digitCount * 10 + containNumber
                    if leftOver >= closest {
                        sum += 1
                    }
                } else if digitCount == containNumber {

                    let zeroStart: Int = containNumber * 10
                    let doubleCase: Int = zeroStart + containNumber
                    sum += leftOver - zeroStart + 1
                    if targetNumber >= doubleCase {
                        sum += 1
                    }
                } else if digitCount > containNumber {

                    sum += 10
                    let closest: Int = digitCount * 10 + containNumber
                    if leftOver >= closest {
                        sum += 1
                    }
                }
            }
        }
        return sum
    }

    func calculateCount(_ count: Int, _ containNumber: Int, _ powVal: Int, _ digitBase: Int) -> Int {

        var sum: Int = 0
        // Ex: 700, 0~99 .... 600~699
        sum += count * digitBase
        // Ex: 700 or 7000
        if count == containNumber {
            sum += 1
        }
        // Ex: 800 or 8000
        if count > containNumber {
            sum += powVal
        }
        return sum
    }

    // For 0 ~9 -> 1, 0~99 -> 20, 0~999 -> 300, 0~9999 -> 4000
    func calculateDigitBase(_ digit: Int, _ base: Int, _ containNumber: Int, _ targetNumber: Int) -> Int {

        if digit == 1 {
            return 1
        }
        let power: Int = Int(pow(10.0, Float(digit) - 1))
        return Int(base * 10) + power
    }
}
