//
//  AppDelegate.swift
//  FontPicker
//
//  Created by apple on 2021/7/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        if let window = window {
            let navigator: UINavigationController = window.rootViewController as! UINavigationController
            navigator.viewControllers = [FontPickerRouter.createView()]
        }
        return true
    }
}
