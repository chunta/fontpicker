//
//  PickerViewModel.swift
//  FontPicker
//
//  Created by apple on 2021/7/21.
//

import Foundation
import UIKit

enum FontFamilyState {
    case requesting
    case owned
}

protocol IFontPickerView: AnyObject {
    func requestFontListDone()
    func requestFontListFail()
    func fontDidRegistedAtItemIndex(_ index: Int)
    func fontRegistrationFailed(_ index: Int)
}

class FontPickerViewModel {

    weak var view: IFontPickerView!

    private var remoteItemList: [FontItem] = []

    private let apiKey: String = "AIzaSyBDPNnlbJbAtKyW68QlQFNOB_wA32p9Tbc"
    private let localCacheFontLimit: Int = 40
    private let localFontListFileName: String = "localFontList.plist"
    private var localItemList: [FontItem] = []
    private let localCacheLifeTime: Int = 3600 // One hour (second)
    private let defaultFontStyle: String = "regular"
    private var documentURL: URL {
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return path.appendingPathComponent("Font")
    }
    private var requestingMap: Set<Int> = []

    init() {

        createFontFolder()
        listDocumentFiles()
        cleanLocalCache()
        registerFontFromLocal(&localItemList)
    }

    var apiUrl: String {
        return "https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity"
    }

    private func createFontFolder() {

        if !FileManager.default.fileExists(atPath: documentURL.path) {
            do {
                try FileManager.default.createDirectory(atPath: documentURL.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
    }

    private func listDocumentFiles() {

        print("List = Start")
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentURL, includingPropertiesForKeys: nil)
            print(directoryContents)
        } catch {
            print(error)
        }
        print("List = End")
    }

    private func cleanLocalCache() {

        print("[Local cache] clean")
        let url = documentURL.appendingPathComponent(localFontListFileName)
        do {
            typealias Items = [FontItem]
            let data = try Data(contentsOf: url)
            let decoder = PropertyListDecoder()
            localItemList = try decoder.decode(Items.self, from: data)
        } catch {
            print(error)
        }

        // Sort with downloadTimestamp
        localItemList.sort(by: {$0.lastDownload! < $1.lastDownload!})

        // Remove item which is too old
        var removalList: [Int] = []
        let now = Date()
        let nowTimestamp: Int = Int(now.timeIntervalSince1970)
        for index in 0..<self.localItemList.count {
            print("[Local cache] cached item: \(localItemList[index].family) \(localItemList[index].lastDownload ?? 0)")
            let dif: Int = nowTimestamp - localItemList[index].lastDownload!
            if dif >= localCacheLifeTime {
                removalList.append(index)
            }
        }
        removalList.reverse()
        for index in 0..<removalList.count {
            let family: String = localItemList[removalList[index]].family
            let fileExtension: String = localItemList[removalList[index]].fileExtension!
            let fullName: String = String.init(format: "%@.%@", family, fileExtension)
            let fileURL: URL = documentURL.appendingPathComponent(fullName)
            let filePath: String = fileURL.path
            var removalResult: Bool = true
            do {
                let fileManager = FileManager.default
                if fileManager.fileExists(atPath: filePath) {
                    try fileManager.removeItem(atPath: filePath)
                }
            } catch {
                print(error)
                removalResult = false
            }
            if removalResult {
                print("[Local cache] remove local cached item: \(localItemList[removalList[index]].family)")
                localItemList.remove(at: removalList[index])
            }
        }

        // Remove item if over the limit
        if localItemList.count > localCacheFontLimit {
            let dif: Int = localItemList.count - localCacheFontLimit
            let endIndex = localItemList.index(localItemList.startIndex, offsetBy: dif)
            localItemList.removeSubrange(localItemList.startIndex..<endIndex)
        }

        archiveItemList(&localItemList, localFontListFileName)
    }

    private func registerFontFromLocal(_ itemList: inout [FontItem]) {

        for index in 0..<itemList.count {
            let family: String = itemList[index].family
            let fileExtension: String = itemList[index].fileExtension!
            let fullName: String = String.init(format: "%@.%@", family, fileExtension)
            let fileURL = (documentURL.appendingPathComponent(fullName))
            let result: Bool = registerFontWithURL(family, fileURL)
            print("[Font registeration] font: \(family), registered: \(result)")
        }
    }

    func requestFontList(_ url: URL? = nil) {

        if let data = UserDefaults.standard.value(forKey: apiUrl) as? Data {
            do {
                self.remoteItemList = try PropertyListDecoder().decode([FontItem].self, from: data)
                self.view.requestFontListDone()
                return
            } catch {
                print(error)
            }
        }

        let urlString: String = String.init(format: "%@&key=%@", apiUrl, apiKey)
        let url: URL = url ?? URL.init(string: urlString)!
        let request: URLRequest = URLRequest.init(url: url)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let httpResponse = response as? HTTPURLResponse {
                print("Code:\(httpResponse.statusCode)")
                if httpResponse.statusCode != 200 {
                    self.view.requestFontListFail()
                    return
                }
            }
            if error != nil {
                print("Error \(error.debugDescription)")
                DispatchQueue.main.async {
                    self.view.requestFontListFail()
                }
                return
            }
            let decoder = JSONDecoder()
            do {
                let response: FontResponse = try decoder.decode(FontResponse.self, from: data!)
                self.remoteItemList = response.items
                var removalIndices: [Int] = []
                for index in 0..<self.remoteItemList.count {
                    var fileExtension: String?
                    if let urlString: String = self.remoteItemList[index].files[self.defaultFontStyle] {
                        fileExtension = URL(fileURLWithPath: urlString).pathExtension
                    }
                    if let fileExtension = fileExtension {
                        self.remoteItemList[index].fileExtension = fileExtension
                    } else {
                        print("[JSON response] remove index at:\(self.remoteItemList[index].family)")
                        removalIndices.append(index)
                    }
                }
                print("[JSON response] count before removal: \(self.remoteItemList.count)")
                removalIndices.reverse()
                for index in 0..<removalIndices.count {
                    self.remoteItemList.remove(at: removalIndices[index])
                }
                print("[JSON response] count after removal: \(self.remoteItemList.count)")

                // Save the remoteItemList locally as cache
                UserDefaults.standard.set(try PropertyListEncoder().encode(self.remoteItemList), forKey: self.apiUrl)
            } catch {
                print("Catch \(error.localizedDescription)")
            }
            DispatchQueue.main.async {
                self.view.requestFontListDone()
            }
        }.resume()
    }

    func itemCount() -> Int {

        return remoteItemList.count
    }

    func itemAtItemIndex(_ itemIndex: Int) -> FontItem {

        return remoteItemList[itemIndex]
    }

    func fontAtItemIndex(_ itemIndex: Int) -> UIFont? {

        // Check supported fonts
        let item: FontItem = remoteItemList[itemIndex]
        let font: UIFont? = UIFont.init(name: item.family, size: 2)
        return font
    }

    func requestFontAtItemIndex(_ itemIndex: Int) -> FontFamilyState {

        if fontAtItemIndex(itemIndex) != nil {
            return .owned
        }
        if requestingMap.contains(itemIndex) {
            print("Same request is being executed....")
            return .requesting
        }
        downloadFontForItemAtIndex(itemIndex) { result in
            if result {
                var item: FontItem = self.remoteItemList[itemIndex]
                let now = Date()
                item.lastDownload = Int(now.timeIntervalSince1970)
                print(item.lastDownload!)
                self.localItemList.append(item)
                self.archiveItemList(&self.localItemList, self.localFontListFileName)
            }
            self.requestingMap.remove(itemIndex)
        }
        requestingMap.insert(itemIndex)
        return .requesting
    }

    private func archiveItemList(_ itemList: inout [FontItem], _ plistFileName: String) {

        let url = documentURL.appendingPathComponent(plistFileName)
        let encoder = PropertyListEncoder()
        do {
            let data = try encoder.encode(itemList)
            try data.write(to: url)
        } catch {
            print(error)
        }
    }

    private func downloadFontForItemAtIndex(_ itemIndex: Int,
                                            _ completionHandler: @escaping ((_ success: Bool) -> Void)) {

        print("downloadFontForItemAtIndex \(itemIndex)")
        let item: FontItem = remoteItemList[itemIndex]
        let urlString: String = item.files[defaultFontStyle]!
        let url: URL = URL.init(string: urlString)!
        let fileExtension: String = URL(fileURLWithPath: urlString).pathExtension
        let request: URLRequest = URLRequest.init(url: url)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                print("request font data fail: \(error.debugDescription)")
                return
            }
            let registered: Bool = self.registerWithFontData(item.family, fileExtension, data!)
            DispatchQueue.main.async {
                if registered {
                    self.view.fontDidRegistedAtItemIndex(itemIndex)
                } else {
                    self.view.fontRegistrationFailed(itemIndex)
                }
                completionHandler(registered)
            }
        }.resume()
    }

    private func registerWithFontData(_ familyName: String,
                                      _ fileExtension: String,
                                      _ data: Data) -> Bool {
        print("registerWithFontData")
        let fileURL = documentURL.appendingPathComponent(String.init(format: "%@.%@", familyName, fileExtension))
        do {
            try data.write(to: fileURL, options: .atomic)
        } catch {
            print(error)
        }
        var error: Unmanaged<CFError>?
        guard let provider: CGDataProvider = CGDataProvider(data: data as CFData) else {
            return false
        }
        guard let fontRef: CGFont = CGFont(provider) else {
            return false
        }
        if CTFontManagerRegisterGraphicsFont(fontRef, &error) == false {
            let message = error.debugDescription
            print(message)
            error?.release()
            return false
        }
        guard let fontName: CFString = fontRef.postScriptName else {
            print("Cannot get fontName")
            return false
        }
        print(fontName)
        return true
    }

    private func registerFontWithURL(_ familyName: String, _ fontUrl: URL) -> Bool {

        do {
            let data = try Data(contentsOf: fontUrl)
            var error: Unmanaged<CFError>?
            guard let provider: CGDataProvider = CGDataProvider(data: data as CFData) else {
                return false
            }
            guard let fontRef: CGFont = CGFont(provider) else {
                return false
            }
            if CTFontManagerRegisterGraphicsFont(fontRef, &error) == false {
                let message = error.debugDescription
                print("[Font Registeration] \(message)")
                error?.release()
                return false
            }
            if fontRef.postScriptName == nil {
                print("[Font Registeration] cannot get postScriptName from font: \(familyName)")
                return false
            }
        } catch {
            print(error)
            return false
        }
        return true
    }
}
