//
//  FontPickerCell.swift
//  FontPicker
//
//  Created by apple on 2021/7/22.
//

import UIKit

class FontPickerCell: UICollectionViewCell {

    static let cellId: String = "FontPickerCell"

    var spinner: UIActivityIndicatorView!
    var checkIcon: UIImageView!
    var demoLabel: UILabel!
    var downloadIcon: UIImageView!
    var downloadOverlay: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)

        spinner = UIActivityIndicatorView.init(frame: .zero)
        spinner.assignColor(Style.colorActivityIndicator)
        spinner.style = .whiteLarge
        self.contentView.addSubview(spinner)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
            spinner.widthAnchor.constraint(equalToConstant: Style.IconSizeMedium.width),
            spinner.heightAnchor.constraint(equalToConstant: Style.IconSizeMedium.height)
        ])

        checkIcon = UIImageView.init(image: UIImage.init(named: "Check"))
        self.contentView.addSubview(checkIcon)
        checkIcon.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            checkIcon.widthAnchor.constraint(equalToConstant: Style.iconSizeSmall.width),
            checkIcon.heightAnchor.constraint(equalToConstant: Style.iconSizeSmall.height),
            checkIcon.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: Style.spaceSmall),
            checkIcon.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -Style.spaceSmall)
        ])

        demoLabel = UILabel.init(frame: .zero)
        self.contentView.addSubview(demoLabel)
        demoLabel.translatesAutoresizingMaskIntoConstraints = false
        demoLabel.text = "Demo"
        demoLabel.textAlignment = .center
        demoLabel.minimumScaleFactor = Style.fontMinScale
        demoLabel.adjustsFontSizeToFitWidth = true
        NSLayoutConstraint.activate([
            demoLabel.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
            demoLabel.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
            demoLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: Style.spaceSmall),
            demoLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -Style.spaceSmall)
        ])

        downloadOverlay = UIView.init(frame: .zero)
        downloadOverlay.backgroundColor = Style.colorLightGreyTransparent
        self.contentView.addSubview(downloadOverlay)
        downloadOverlay.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            downloadOverlay.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
            downloadOverlay.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
            downloadOverlay.widthAnchor.constraint(equalTo: self.contentView.widthAnchor),
            downloadOverlay.heightAnchor.constraint(equalTo: self.contentView.heightAnchor)
        ])

        downloadIcon = UIImageView.init(image: UIImage.init(named: "Download"))
        self.contentView.addSubview(downloadIcon)
        downloadIcon.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            downloadIcon.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
            downloadIcon.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
            downloadIcon.widthAnchor.constraint(equalToConstant: Style.iconSizeSmall.width),
            downloadIcon.heightAnchor.constraint(equalToConstant: Style.iconSizeSmall.height)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func fillData(_ item: FontItem, _ font: UIFont?, _ index: Int, _ selectedIndex: Int) {

        // Reset to default since the recycle pool
        checkIcon.isHidden = !(selectedIndex == index)
        demoLabel.font = Style.fontLarge
        demoLabel.text = item.family
        downloadOverlay.isHidden = false
        downloadIcon.isHidden = false

        if font != nil {
            print("Index: \(index) Has font ready: \(item.family)")
            demoLabel.font = font!.withSize(Style.fontSizeLarge)
            downloadOverlay.isHidden = true
            downloadIcon.isHidden = true
        }
    }

    func requestFontStart() {

        print("request font start...")
        downloadIcon.isHidden = true
        spinner.startAnimating()
    }

    func requestFontFail() {

        print("request font fail...")
        downloadIcon.isHidden = false
        spinner.stopAnimating()
    }
}
