//
//  FontPickerRouter.swift
//  FontPicker
//
//  Created by apple on 2021/7/21.
//

import Foundation

class FontPickerRouter {

    static func createView() -> FontPickerView {

        let pickerView: FontPickerView = FontPickerView.init()
        let pickerViewModel: FontPickerViewModel = FontPickerViewModel.init()
        pickerView.viewModel = pickerViewModel
        pickerViewModel.view = pickerView
        return pickerView
    }
}
