//
//  SolutionBF.swift
//  FontPicker
//
//  Created by apple on 2021/7/27.
//

import Foundation

class SolutionBf {

    private func substr(_ source: String, _ index: Int) -> String {

        let index = source.index(source.startIndex, offsetBy: index)
        return String(source[index...index])
    }

    func bruceForce(_ c: Int, _ n: Int) -> Int {

        var sum: Int = 0
        for index in 1...n {
            let numberStr: String = String.init(format: "%d", index)
            for jdex in 0..<numberStr.count {
                let sub: String = substr(numberStr, jdex)
                if Int(sub) == c {
                    sum += 1
                }
            }
        }
        return sum
    }
}
